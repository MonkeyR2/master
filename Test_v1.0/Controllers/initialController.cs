﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Test_v1._0.Models;

namespace Test_v1._0.Controllers
{
    public class initialController : ApiController
    {
        // GET: api/initial/GetAllData
        public string GetAllData()
        {
            //WARNING: THIS MIGHT TAKES A VERY LONG TIME TO PROCESS
            InitialDataModel.retrieveData(95);

            return "success retrieving all data";
        }

        // GET: api/initial/GetDataByTotalPage/5
        //id = total page data to be retrieve
        public string GetDataByTotalPage(int id)
        {
            //WARNING: THIS MIGHT TAKES A VERY LONG TIME TO PROCESS
            InitialDataModel.retrieveData(id);

            return "success retrieving data for " + id + " pages";
        }
    }
}
