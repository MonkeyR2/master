﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using Test_v1._0.Models;

namespace Test_v1._0.Controllers
{
    public class memeController : ApiController
    {
        //Test on local pc file
        string filePath = WebConfigurationManager.AppSettings["FilePath"];
        string urlPath = WebConfigurationManager.AppSettings["UrlPath"];

        #region "local file"
        [HttpGet]
        public List<InitialDataModel> all()
        {
            List<InitialDataModel> fullList = new List<InitialDataModel>();

            try
            {
                string result = "";

                using (StreamReader r = new StreamReader(filePath))
                {
                    string json = r.ReadToEnd();
                    fullList = JsonConvert.DeserializeObject<List<InitialDataModel>>(json);

                    //edit request count
                    dynamic jsonObj = JsonConvert.DeserializeObject(json);
                    for (int count = 0; count < jsonObj.Count; count++)
                    {
                        jsonObj[count]["requestCount"] = jsonObj[count]["requestCount"] + 1;
                    }
                    result = JsonConvert.SerializeObject(jsonObj, Formatting.Indented);
                }

                File.WriteAllText(filePath, result);
            }
            catch (Exception ex) { }

            return fullList;
        }

        [HttpGet]
        public List<InitialDataModel> id(int id)
        {
            List<InitialDataModel> fullList = new List<InitialDataModel>();
            List<InitialDataModel> tempItem = new List<InitialDataModel>();

            try
            {
                string result = "";

                using (StreamReader r = new StreamReader(filePath))
                {
                    string json = r.ReadToEnd();
                    fullList = JsonConvert.DeserializeObject<List<InitialDataModel>>(json);
                    tempItem.Add(fullList[id - 1]);

                    //edit request count
                    dynamic jsonObj = JsonConvert.DeserializeObject(json);
                    jsonObj[id - 1]["requestCount"] = jsonObj[id - 1]["requestCount"] + 1;
                    result = JsonConvert.SerializeObject(jsonObj, Formatting.Indented);
                }

                File.WriteAllText(filePath, result);
            }
            catch (Exception ex) { }

            return tempItem;
        }

        [HttpGet]
        public List<InitialDataModel> page(int id)
        {
            List<InitialDataModel> fullList = new List<InitialDataModel>();
            List<InitialDataModel> tempItems = new List<InitialDataModel>();

            try
            {
                string result = "";

                using (StreamReader r = new StreamReader(filePath))
                {
                    string json = r.ReadToEnd();
                    fullList = JsonConvert.DeserializeObject<List<InitialDataModel>>(json);

                    //edit request count
                    dynamic jsonObj = JsonConvert.DeserializeObject(json);
                    //loop to check page
                    for (int count = 0; count < jsonObj.Count; count++)
                    {
                        if (jsonObj[count]["page"] == id)
                        {
                            tempItems.Add(fullList[count]);
                            jsonObj[count]["requestCount"] = jsonObj[count]["requestCount"] + 1;
                        }
                    }
                    result = JsonConvert.SerializeObject(jsonObj, Formatting.Indented);
                }

                File.WriteAllText(filePath, result);
            }
            catch (Exception ex) { }

            return tempItems;
        }

        [HttpGet]
        public List<InitialDataModel> popular()
        {
            List<InitialDataModel> fullList = new List<InitialDataModel>();
            List<InitialDataModel> tempItems = new List<InitialDataModel>();

            try
            {
                string result = "";
                int maxRequestCount = 0;

                using (StreamReader r = new StreamReader(filePath))
                {
                    string json = r.ReadToEnd();
                    fullList = JsonConvert.DeserializeObject<List<InitialDataModel>>(json);

                    //edit request count
                    dynamic jsonObj = JsonConvert.DeserializeObject(json);
                    //loop to check maximum value
                    for (int count = 0; count < jsonObj.Count; count++)
                    {
                        if (jsonObj[count]["requestCount"] > maxRequestCount)
                        {
                            maxRequestCount = jsonObj[count]["requestCount"];
                        }
                    }
                    //loop to check index of popular
                    for (int count = 0; count < jsonObj.Count; count++)
                    {
                        if (jsonObj[count]["requestCount"] == maxRequestCount)
                        {
                            tempItems.Add(fullList[count]);
                            jsonObj[count]["requestCount"] = jsonObj[count]["requestCount"] + 1;
                        }
                    }
                    result = JsonConvert.SerializeObject(jsonObj, Formatting.Indented);
                }

                File.WriteAllText(filePath, result);
            }
            catch (Exception ex) { }

            return tempItems;
        }

        [HttpPost]
        public async Task<string> create()
        {
            List<InitialDataModel> fullList = new List<InitialDataModel>();

            try
            {
                string result = await Request.Content.ReadAsStringAsync();
                dynamic jsonObj = JsonConvert.DeserializeObject(result);
                var obj = jsonObj["data"];

                if (obj.Count > 0)
                {
                    using (StreamReader r = new StreamReader(filePath))
                    {
                        string json = r.ReadToEnd();
                        fullList = JsonConvert.DeserializeObject<List<InitialDataModel>>(json);

                        for (int count = 0; count < obj.Count; count++)
                        {
                            int tempPageCount = fullList.Count / 9 + 1;

                            fullList.Add(new InitialDataModel()
                            {
                                id = fullList.Count + 1,
                                name = obj[count]["name"],
                                url = obj[count]["url"],
                                page = tempPageCount,
                                requestCount = 0
                            });
                        }
                    }

                    using (StreamWriter file = File.CreateText(WebConfigurationManager.AppSettings["FilePath"]))
                    {
                        JsonSerializer serializer = new JsonSerializer();
                        //serialize object directly into file stream
                        serializer.Serialize(file, fullList);
                    }
                }
            }
            catch (Exception ex) { return "fail: " + ex; }

            return "successful added";
        }
        #endregion

        #region "url"
        [HttpGet]
        public List<InitialDataModel> all2()
        {
            List<InitialDataModel> fullList = new List<InitialDataModel>();

            try
            {
                using (WebClient r = new WebClient())
                {
                    string json = "";
                    HttpCookie myCookie = HttpContext.Current.Request.Cookies["fulldata"];

                    if (myCookie == null || myCookie.Value == "")
                    {
                        json = r.DownloadString(urlPath);

                        HttpCookie newCookie = new HttpCookie("fulldata");
                        newCookie.Value = json;
                        newCookie.Expires = DateTime.Now.AddDays(1d);
                        HttpContext.Current.Response.Cookies.Add(newCookie);

                        myCookie = HttpContext.Current.Request.Cookies["fulldata"];
                    }
                    else
                    {
                        json = myCookie.Value;
                    }

                    fullList = JsonConvert.DeserializeObject<List<InitialDataModel>>(json);

                    //edit request count
                    dynamic jsonObj = JsonConvert.DeserializeObject(json);
                    for (int count = 0; count < jsonObj.Count; count++)
                    {
                        jsonObj[count]["requestCount"] = jsonObj[count]["requestCount"] + 1;
                    }
                    string result = JsonConvert.SerializeObject(jsonObj, Formatting.None);

                    HttpCookie tempCookie = new HttpCookie("fulldata");
                    tempCookie.Value = result;
                    tempCookie.Expires = DateTime.Now.AddDays(1d);
                    HttpContext.Current.Response.Cookies.Add(tempCookie);
                }
            }
            catch (Exception ex) { }

            return fullList;
        }

        [HttpGet]
        public List<InitialDataModel> id2(int id)
        {
            List<InitialDataModel> fullList = new List<InitialDataModel>();
            List<InitialDataModel> tempItem = new List<InitialDataModel>();

            try
            {
                using (WebClient r = new WebClient())
                {
                    string json = "";
                    HttpCookie myCookie = HttpContext.Current.Request.Cookies["fulldata"];

                    if (myCookie == null || myCookie.Value == "")
                    {
                        json = r.DownloadString(urlPath);

                        HttpCookie newCookie = new HttpCookie("fulldata");
                        newCookie.Value = json;
                        newCookie.Expires = DateTime.Now.AddDays(1d);
                        HttpContext.Current.Response.Cookies.Add(newCookie);

                        myCookie = HttpContext.Current.Request.Cookies["fulldata"];
                    }
                    else
                    {
                        json = myCookie.Value;
                    }

                    fullList = JsonConvert.DeserializeObject<List<InitialDataModel>>(json);
                    tempItem.Add(fullList[id - 1]);

                    //edit request count
                    dynamic jsonObj = JsonConvert.DeserializeObject(json);
                    jsonObj[id - 1]["requestCount"] = jsonObj[id - 1]["requestCount"] + 1;
                    string result = JsonConvert.SerializeObject(jsonObj, Formatting.None);

                    HttpCookie tempCookie = new HttpCookie("fulldata");
                    tempCookie.Value = result;
                    tempCookie.Expires = DateTime.Now.AddDays(1d);
                    HttpContext.Current.Response.Cookies.Add(tempCookie);
                }
            }
            catch (Exception ex) { }

            return tempItem;
        }

        [HttpGet]
        public List<InitialDataModel> page2(int id)
        {
            List<InitialDataModel> fullList = new List<InitialDataModel>();
            List<InitialDataModel> tempItems = new List<InitialDataModel>();

            try
            {
                using (WebClient r = new WebClient())
                {
                    string json = "";
                    HttpCookie myCookie = HttpContext.Current.Request.Cookies["fulldata"];

                    if (myCookie == null || myCookie.Value == "")
                    {
                        json = r.DownloadString(urlPath);

                        HttpCookie newCookie = new HttpCookie("fulldata");
                        newCookie.Value = json;
                        newCookie.Expires = DateTime.Now.AddDays(1d);
                        HttpContext.Current.Response.Cookies.Add(newCookie);

                        myCookie = HttpContext.Current.Request.Cookies["fulldata"];
                    }
                    else
                    {
                        json = myCookie.Value;
                    }

                    fullList = JsonConvert.DeserializeObject<List<InitialDataModel>>(json);

                    //edit request count
                    dynamic jsonObj = JsonConvert.DeserializeObject(json);
                    //loop to check page
                    for (int count = 0; count < jsonObj.Count; count++)
                    {
                        if (jsonObj[count]["page"] == id)
                        {
                            tempItems.Add(fullList[count]);
                            jsonObj[count]["requestCount"] = jsonObj[count]["requestCount"] + 1;
                        }
                    }
                    string result = JsonConvert.SerializeObject(jsonObj, Formatting.None);

                    HttpCookie tempCookie = new HttpCookie("fulldata");
                    tempCookie.Value = result;
                    tempCookie.Expires = DateTime.Now.AddDays(1d);
                    HttpContext.Current.Response.Cookies.Add(tempCookie);
                }
            }
            catch (Exception ex) { }

            return tempItems;
        }

        [HttpGet]
        public List<InitialDataModel> popular2()
        {
            List<InitialDataModel> fullList = new List<InitialDataModel>();
            List<InitialDataModel> tempItems = new List<InitialDataModel>();

            try
            {
                int maxRequestCount = 0;

                using (WebClient r = new WebClient())
                {
                    string json = "";
                    HttpCookie myCookie = HttpContext.Current.Request.Cookies["fulldata"];

                    if (myCookie == null || myCookie.Value == "")
                    {
                        json = r.DownloadString(urlPath);

                        HttpCookie newCookie = new HttpCookie("fulldata");
                        newCookie.Value = json;
                        newCookie.Expires = DateTime.Now.AddDays(1d);
                        HttpContext.Current.Response.Cookies.Add(newCookie);

                        myCookie = HttpContext.Current.Request.Cookies["fulldata"];
                    }
                    else
                    {
                        json = myCookie.Value;
                    }

                    fullList = JsonConvert.DeserializeObject<List<InitialDataModel>>(json);

                    //edit request count
                    dynamic jsonObj = JsonConvert.DeserializeObject(json);
                    //loop to check maximum value
                    for (int count = 0; count < jsonObj.Count; count++)
                    {
                        if (jsonObj[count]["requestCount"] > maxRequestCount)
                        {
                            maxRequestCount = jsonObj[count]["requestCount"];
                        }
                    }
                    //loop to check index of popular
                    for (int count = 0; count < jsonObj.Count; count++)
                    {
                        if (jsonObj[count]["requestCount"] == maxRequestCount)
                        {
                            tempItems.Add(fullList[count]);
                            jsonObj[count]["requestCount"] = jsonObj[count]["requestCount"] + 1;
                        }
                    }
                    string result = JsonConvert.SerializeObject(jsonObj, Formatting.None);

                    HttpCookie tempCookie = new HttpCookie("fulldata");
                    tempCookie.Value = result;
                    tempCookie.Expires = DateTime.Now.AddDays(1d);
                    HttpContext.Current.Response.Cookies.Add(tempCookie);
                }
            }
            catch (Exception ex) { }

            return tempItems;
        }

        [HttpPost]
        public async Task<string> create2()
        {
            List<InitialDataModel> fullList = new List<InitialDataModel>();
            string returnResult = "";

            try
            {
                string result = await Request.Content.ReadAsStringAsync();
                dynamic jsonObj = JsonConvert.DeserializeObject(result);
                var obj = jsonObj["data"];

                if (obj.Count > 0)
                {
                    using (WebClient r = new WebClient())
                    {
                        string json = "";
                        HttpCookie myCookie = HttpContext.Current.Request.Cookies["fulldata"];

                        if (myCookie == null || myCookie.Value == "")
                        {
                            json = r.DownloadString(urlPath);

                            HttpCookie newCookie = new HttpCookie("fulldata");
                            newCookie.Value = json;
                            newCookie.Expires = DateTime.Now.AddDays(1d);
                            HttpContext.Current.Response.Cookies.Add(newCookie);

                            myCookie = HttpContext.Current.Request.Cookies["fulldata"];
                        }
                        else
                        {
                            json = myCookie.Value;
                        }

                        fullList = JsonConvert.DeserializeObject<List<InitialDataModel>>(json);

                        for (int count = 0; count < obj.Count; count++)
                        {
                            int tempPageCount = fullList.Count / 9 + 1;

                            fullList.Add(new InitialDataModel()
                            {
                                id = fullList.Count + 1,
                                name = obj[count]["name"],
                                url = obj[count]["url"],
                                page = tempPageCount,
                                requestCount = 0
                            });
                        }
                    }
                    returnResult = JsonConvert.SerializeObject(fullList, Formatting.None);

                    HttpCookie tempCookie = new HttpCookie("fulldata");
                    tempCookie.Value = returnResult;
                    tempCookie.Expires = DateTime.Now.AddDays(1d);
                    HttpContext.Current.Response.Cookies.Add(tempCookie);
                }
            }
            catch (Exception ex) { return "fail: " + ex; }

            return returnResult;
        }
        #endregion
    }
}
