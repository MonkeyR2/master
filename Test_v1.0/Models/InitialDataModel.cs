﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Configuration;
using System.Web.UI.HtmlControls;
using HtmlAgilityPack;
using Newtonsoft.Json;

namespace Test_v1._0.Models
{
    public class InitialDataModel
    {
        public long id { get; set; }
        public string name { get; set; }
        public string url { get; set; }
        public int page { get; set; }
        public int requestCount { get; set; }

        public static void retrieveData(int pageTotalCount)
        {
            try
            {
                List<string> srcTags = new List<string>();
                List<string> nameTags = new List<string>();
                List<int> page = new List<int>();

                for (int count = 1; count <= pageTotalCount; count++)
                {
                    //using HtmlAgilityPack to get the HTML
                    string url = "http://interview.funplay8.com/index.php?page=" + count;
                    var web = new HtmlWeb();
                    var doc = web.Load(url);

                    //retrieving img source path
                    foreach (HtmlNode src in doc.DocumentNode.SelectNodes("//img[@src]"))
                    {
                        HtmlAttribute att = src.Attributes["src"];
                        srcTags.Add(att.Value);
                        page.Add(count);
                    }

                    //retrieving name
                    foreach (HtmlNode name in doc.DocumentNode.SelectNodes("//div[@class='meme-name']//h6"))
                    {
                        nameTags.Add(name.InnerText);
                    }
                }

                if (srcTags.Count == nameTags.Count && nameTags.Count == page.Count && srcTags.Count > 0)
                {
                    //store data to file in Json format
                    storeDataToJson(srcTags, nameTags, page);
                }
            }
            catch (Exception ex) { }
        }

        public static void storeDataToJson(List<string> srcTags, List<string> nameTags, List<int> pageCount)
        {
            try
            {
                List<InitialDataModel> _data = new List<InitialDataModel>();

                for (int count = 0; count < srcTags.Count; count++)
                {
                    _data.Add(new InitialDataModel()
                    {
                        id = count + 1,
                        name = nameTags[count],
                        url = srcTags[count],
                        page = pageCount[count],
                        requestCount = 0
                    });
                }

                //open file stream
                using (StreamWriter file = File.CreateText(WebConfigurationManager.AppSettings["FilePath"]))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    //serialize object directly into file stream
                    serializer.Serialize(file, _data);
                }
            }
            catch (Exception ex) { }
        }
    }
}